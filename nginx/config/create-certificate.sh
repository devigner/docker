#!/bin/sh

DOMAIN=$1

openssl req \
    -x509 \
    -out ${DOMAIN}.crt \
    -keyout ${DOMAIN}.key \
    -newkey rsa:2048 \
    -nodes \
    -sha256 \
    -subj '/CN=*.$DOMAIN' \
    -extensions EXT \
    -config <( printf "[dn]\nCN=*.%s\n[req]\ndistinguished_name = dn\n[EXT]\nsubjectAltName=DNS:*.%s\nkeyUsage=digitalSignature\nextendedKeyUsage=serverAuth" "$DOMAIN" )
