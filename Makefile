SHELL := /bin/bash
OS := $(shell uname)

REGISTRY=registry.gitlab.com/devigner/docker

PHP_VERSIONS="7.3.6" "7.4.rc"
NGINX_VERSIONS="1.15.8"

build:
	@docker login -u gitlab-ci-token -p ${CI_JOB_TOKEN} ${CI_REGISTRY}
	make build-all-php-fpm
	make build-all-nginx

build-all-php-fpm:
	@for PHP_VERSION in $(PHP_VERSIONS); do \
		make build-php-fpm PHP_VERSION=$$PHP_VERSION; \
	done

build-all-nginx:
	@for NGINX_VERSION in $(NGINX_VERSIONS); do \
		make build-nginx NGINX_VERSION=$$NGINX_VERSION; \
	done

build-php-fpm:
	make _build-set TYPE=php-fpm-alpine VERSION=${PHP_VERSION}

build-nginx:
	make _build-set TYPE=nginx VERSION=${NGINX_VERSION}

_build-set:
	make _build-target TARGET=base
	make _build-target TARGET=symfony
	make _build-target TARGET=symfony-xdebug

_build-target:
	@if [ -f ./${TYPE}/${VERSION}/${TARGET}/Dockerfile ]; then \
		docker build -f ${TYPE}/${VERSION}/${TARGET}/Dockerfile -t ${REGISTRY}/${TARGET}/${TYPE}:${VERSION} .; \
	fi

	@if [ -f ./${TYPE}/${VERSION}/${TARGET}/Dockerfile ]; then \
		docker push ${REGISTRY}/${TARGET}/${TYPE}:${VERSION};\
	fi
